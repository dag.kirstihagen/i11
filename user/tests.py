from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from projects.models import ProjectCategory


class TestTwoWayDomain(TestCase):

    def setUp(self):
        self.client = Client()
        self.url = reverse('signup')
        ProjectCategory.objects.create(name='1').save()
        ProjectCategory.objects.create(name='2').save()

    def test_valid_input(self):
        """ Valid input, matching passwords """

        self.client.post(self.url, {
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        self.assertEqual(User.objects.all().count(), 1)

    def test_password_fail(self):
        """ Invalid input, mismatching passwords """

        self.client.post(self.url, {
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'testpassword',
            'password2': 'test_password',
        })

        self.assertEqual(User.objects.all().count(), 0)

    def test_email(self):
        """ Valid input, matching emails """

        self.client.post(self.url, {
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        self.assertEqual(User.objects.all().count(), 1)

    def test_email_fail(self):
        """ Invalid input, mismatching emails """

        self.client.post(self.url, {
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@mismatch.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        # self.assertEqual(User.objects.all().count(), 0)

    def test_username_password(self):
        """ Invalid input, mismatching emails """

        self.client.post(self.url, {
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@mismatch.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'test_password',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        self.assertEqual(User.objects.all().count(), 0)

