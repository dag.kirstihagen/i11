from django.test import TestCase, Client
from .forms import TaskOfferForm
from user.forms import SignUpForm
from projects.models import Project, Task, TaskOffer, ProjectCategory, Rating, Team
from user.models import Profile
from django.contrib.auth.models import User
from django.urls import reverse
from .views import get_user_task_permissions


class TestProjectViews(TestCase):
    """ This class tests project_view and get_user_task_permissions in project/view.py. """

    def setUp(self):
        # Create user without hashed password.
        self.user_customer = User.objects.create(username='customer', is_active=True)
        self.user_customer.set_password('password')
        self.user_customer.save()
        self.customer = Profile(id=1, user=self.user_customer)

        # Create user without hashed password.
        self.user_project_manager = User.objects.create(username='project_manager', is_active=True)
        self.user_project_manager.set_password('password')
        self.user_project_manager.save()
        self.project_manager = Profile(id=2, user=self.user_project_manager)

        # Create user without hashed password.
        self.user_task_participant = User.objects.create(username='task_participant', is_active=True)
        self.user_task_participant.set_password('password')
        self.user_task_participant.save()
        self.task_participant = Profile(id=3, user=self.user_task_participant)

        # Create user without hashed password.
        self.user_not_task_participant = User.objects.create(username='not_task_participant', is_active=True)
        self.user_not_task_participant.set_password('password')
        self.user_not_task_participant.save()
        self.not_task_participant = Profile(id=4, user=self.user_not_task_participant)

        # Create project.
        self.category = ProjectCategory.objects.create(name='name')
        self.project = Project.objects.create(id=1, user_id=self.customer.user_id, title='title',
                                              description='description', category=self.category)
        self.project.save()

        # Create task with a task offer.
        self.task = Task.objects.create(project=self.project, title='title', description='description', budget=100)
        self.task_offer = TaskOffer.objects.create(id=1, task=self.task, title='title', description='description',
                                                   price=100, offerer_id=self.project_manager.user_id)
        self.task_offer.save()

        # Create the client and log in as the customer.
        self.client = Client()
        self.client.login(username='customer', password='password')

        # URL to project_view with self.project.id as argument.
        self.project_url = reverse('project_view', args='1')

    """
        The following section of tests runs get_user_task_permissions(user, task) from projects/views.py and checks that
        the correct permissions are granted to the task's customer, project manager, participants and non-participants. 
    """

    def test_customer(self):
        """ The customer should have all permissions. """

        # Executes the function with the customer as input.
        result = get_user_task_permissions(self.user_customer, self.task)

        # Assumed result.
        customer_permissions = {'write': True, 'read': True, 'modify': True, 'owner': True, 'upload': True}

        # Check that the result is correct.
        self.assertEqual(result, customer_permissions, 'Permissions granted to the customer.')

    def test_project_manager(self):
        """ The project manager should have all permissions except 'owner'. """

        # Accepts the task offer from the project manager.
        self.task_offer.status = 'a'
        self.task_offer.save()

        # Executes the function with the project manager as input.
        result = get_user_task_permissions(self.user_project_manager, self.task)

        # Assumed result.
        project_manager_permissions = {'write': True, 'read': True, 'modify': True, 'owner': False, 'upload': True}

        # Check that the result is correct.
        self.assertEqual(result, project_manager_permissions, 'Permissions granted to the project manager.')

    def test_task_participant(self):
        """ A task participant should have the permissions granted to him. """

        # Add task participant to the team: team members get view_task permission, write=True grants upload permission.
        Team.objects.create(name='name', task=self.task, write=True).members.add(self.task_participant)

        # Grant task participant read, write and modify permissions.
        self.task.read.add(self.task_participant)
        self.task.write.add(self.task_participant)
        self.task.modify.add(self.task_participant)

        # Executes the function with a task participant that are granted all permissions as input.
        result = get_user_task_permissions(self.user_task_participant, self.task)

        # Assumed result.
        task_participant_permissions = {'write': True, 'read': True, 'modify': True, 'view_task': True, 'owner': False,
                                        'upload': True}

        # Check that the result is correct.
        self.assertEqual(result, task_participant_permissions, 'Permissions granted to a task participant.')

    def test_task_non_participant(self):
        """ A task non-participant should have no permissions. """

        # Executes the function with non-participant as input.
        result = get_user_task_permissions(self.user_not_task_participant, self.task)

        # Assumed result.
        task_non_participant_permissions = {'write': False, 'read': False, 'modify': False, 'view_task': False,
                                            'owner': False, 'upload': False}

        # Check that the result is correct.
        self.assertEqual(result, task_non_participant_permissions,
                         'Permissions granted to a user not participating in the task.')

    """
         The following section of tests runs project_view(request, project_id) from projects/views.py and checks that 
         the correct HTTP status code is returned. The tests also checks that objects are created or edited correctly 
         where it is applicable. 
     """

    def test_offer_response(self):
        """ Sends a valid offer response post request. """

        # An accepted offer has status 'a'.
        offer_accepted = 'a'

        # Valid offer response.
        request = self.client.post(self.project_url, {'offer_response': '', 'taskofferid': self.task_offer.id,
                                                      'status': offer_accepted, 'feedback': 'feedback'})

        # Updates self.task_offer with data from the database.
        self.task_offer.refresh_from_db()

        # Checks that the task offer's status is changed from 'p' (pending) to 'a' (accepted).
        self.assertEqual(self.task_offer.status, offer_accepted, 'The offer should be accepted.')

        # Checks that the project manager is granted read and write permissions.
        self.assertTrue(self.task_offer.task.read.filter(user=self.user_project_manager).exists())
        self.assertTrue(self.task_offer.task.write.filter(user=self.user_project_manager).exists())

        # Checks that the project manager is added to the project participant list.
        self.assertTrue(self.task_offer.task.project.participants.filter(user=self.user_project_manager).exists())

        # Checks that the request is successfully redirecting with status code 302.
        self.assertEqual(request.status_code, 302, 'Post requests in project_view should redirect to project_view.')

    def test_offer_response_failure(self):
        """ Sends an invalid offer response post request: faulty task offer id. """

        # Invalid offer response.
        request = self.client.post(self.project_url, {'offer_response': '', 'taskofferid': 200, 'status': 'a',
                                                      'feedback': 'feedback'})

        # Checks that the request is unsuccessful and returns status code 404.
        self.assertEqual(request.status_code, 404, 'Task offer does not exist.')

    def test_status_change(self):
        """ Sends a valid status change post request for the project. """

        # A finished project has status 'f'.
        project_finished = 'f'

        # Valid status change request.
        request = self.client.post(self.project_url, {'status_change': '', 'status': project_finished})

        # Updates self.project with data from the database.
        self.project.refresh_from_db()

        # Checks that the project's status is changed from 'o' (open) to 'f' (finished).
        self.assertEqual(self.project.status, project_finished)

        # Checks that the request is successfully redirecting with status code 302.
        self.assertEqual(request.status_code, 302, 'Post requests in project_view should redirect to project_view.')

    def test_apply_filter(self):
        """ Sends a valid apply filter get request. """

        # Valid apply filter request.
        request = self.client.get(self.project_url, {'apply_filter': '', 'price-filter': '100', 'rating-filter': '4',
                                                     'order-by': 'oldest'})

        # Checks that the request is successful with status code 200.
        self.assertEqual(request.status_code, 200, 'Get requests in project_view should render project_view.')

    def test_task_review(self):
        """ Sends a valid task review post request. """

        # Valid task review request.
        request = self.client.post(self.project_url,
                                   {'task_review': '', 'task_id': str(self.task.id),
                                    'customer_id': str(self.customer.id),
                                    'project_manager_id': str(self.project_manager.id), 'rating': '4',
                                    'description': 'description'})

        # Gets the number of ratings for the task.
        rating_count = Rating.objects.filter(task_id=self.task.id).count()

        # Checks that the rating object is created.
        self.assertEqual(rating_count, 1)

        # Checks that the request is successfully redirecting with status code 302.
        self.assertEqual(request.status_code, 302, 'Post requests in project_view should redirect to project_view.')

    def test_offer_submit(self):
        """ Sends a valid offer submit post request. """

        # Log in to the project manager's account.
        self.client.logout()
        self.client.login(username='project_manager', password='password')

        # Valid task offer submit request.
        request = self.client.post(self.project_url, {'offer_submit': '', 'taskvalue': self.task.id, 'title': 'title',
                                                      'description': 'description', 'price': 100})

        # Gets the number of task offers for the
        task_offer_count = TaskOffer.objects.filter(task=self.task).count()

        # There exists one task offer object already, so we should now have two of them.
        self.assertEqual(task_offer_count, 2, 'Should have created a new TaskOffer.')

        # Checks that the request is successfully redirecting with status code 302.
        self.assertEqual(request.status_code, 302, 'Post requests in project_view should redirect to project_view.')

    def test_get_project_view(self):
        """ Sends a get request without any additional context data. """

        # Log out from the customer's account.
        self.client.logout()

        # Get request with no additional context data.
        request = self.client.get(self.project_url)

        # Checks that the request is successful with status code 200.
        self.assertEqual(request.status_code, 200, 'Get requests in project_view should render project_view.')


class TestBoundaryValue(TestCase):
    """ This class performs boundary value tests on the input to TaskOfferForm and SignUpForm. """

    def setUp(self):
        # Creates two ProjectCategory object for the input.
        ProjectCategory.objects.create(name='1').save()
        ProjectCategory.objects.create(name='2').save()

    def test_task_offer_title(self):
        """ Tests boundary values for the title field in TaskOfferForm (1-200 characters). """

        title_max_plus = TaskOfferForm(data={
            'title': 'x' * 201,
            'description': 'description',
            'price': 10000
        })

        title_max = TaskOfferForm(data={
            'title': 'x' * 200,
            'description': 'description',
            'price': 10000
        })

        title_max_minus = TaskOfferForm(data={
            'title': 'x' * 199,
            'description': 'description',
            'price': 10000
        })

        title_nom = TaskOfferForm(data={
            'title': 'x' * 100,
            'description': 'description',
            'price': 10000
        })

        title_min_plus = TaskOfferForm(data={
            'title': 'x' * 2,
            'description': 'description',
            'price': 10000
        })

        title_min = TaskOfferForm(data={
            'title': 'x' * 1,
            'description': 'Hello World!',
            'price': 10000
        })

        title_min_minus = TaskOfferForm(data={
            'title': 'x' * 0,
            'description': 'description',
            'price': 10000
        })

        # title_max_plus and title_min_minus should return false.
        self.assertFalse(title_max_plus.is_valid(), 'Acceptable range: 1-200 characters')
        self.assertFalse(title_min_minus.is_valid(), 'Acceptable range: 1-200 characters')

        # title_max, title_max_minus, title_nom, title_min_plus, and title_min should return true.
        self.assertTrue(title_max.is_valid(), 'Acceptable range: 1-200 characters')
        self.assertTrue(title_max_minus.is_valid(), 'Acceptable range: 1-200 characters')
        self.assertTrue(title_nom.is_valid(), 'Acceptable range: 1-200 characters')
        self.assertTrue(title_min_plus.is_valid(), 'Acceptable range: 1-200 characters')
        self.assertTrue(title_min.is_valid(), 'Acceptable range: 1-200 characters')

    def test_task_offer_description(self):
        """ Tests boundary values for the description field in TaskOfferForm (1-500 characters). """

        description_max_plus = TaskOfferForm(data={
            'title': 'title',
            'description': 'x' * 501,
            'price': 10000
        })

        description_max = TaskOfferForm(data={
            'title': 'title',
            'description': 'x' * 500,
            'price': 10000
        })

        description_max_minus = TaskOfferForm(data={
            'title': 'title',
            'description': 'x' * 499,
            'price': 10000
        })

        description_nom = TaskOfferForm(data={
            'title': 'title',
            'description': 'x' * 100,
            'price': 10000
        })

        description_min_plus = TaskOfferForm(data={
            'title': 'title',
            'description': 'x' * 2,
            'price': 10000
        })

        description_min = TaskOfferForm(data={
            'title': 'title',
            'description': 'x' * 1,
            'price': 10000
        })

        description_min_minus = TaskOfferForm(data={
            'title': 'title',
            'description': 'x' * 0,
            'price': 10000
        })

        # description_max_plus and description_min_minus should return false.
        self.assertFalse(description_max_plus.is_valid(), 'Acceptable range: 1-500 characters')
        self.assertFalse(description_min_minus.is_valid(), 'Acceptable range: 1-500 characters')

        # description_max, description_max_minus, description_nom,
        # description_min_plus, and description_min should return true.
        self.assertTrue(description_max.is_valid(), 'Acceptable range: 1-500 characters')
        self.assertTrue(description_max_minus.is_valid(), 'Acceptable range: 1-500 characters')
        self.assertTrue(description_nom.is_valid(), 'Acceptable range: 1-500 characters')
        self.assertTrue(description_min_plus.is_valid(), 'Acceptable range: 1-500 characters')
        self.assertTrue(description_min.is_valid(), 'Acceptable range: 1-500 characters')

    def test_task_offer_price(self):
        """ Tests boundary values for the price field in TaskOfferForm (minimum a single digit number). """

        price_nom = TaskOfferForm(data={
            'title': 'title',
            'description': 'description',
            'price': 10000
        })
        price_min = TaskOfferForm(data={
            'title': 'title',
            'description': 'x' * 500,
            'price': 0
        })
        price_min_minus = TaskOfferForm(data={
            'title': 'title',
            'description': 'x' * 499,
            'price': None
        })

        # price_min_minus should return false.
        self.assertFalse(price_min_minus.is_valid(), 'Minimum one digit number.')

        # price_min and price_nom should return true.
        self.assertTrue(price_nom.is_valid(), 'Minimum one digit number.')
        self.assertTrue(price_min.is_valid(), 'Minimum one digit number.')

    def test_sign_up_first_name(self):
        """ Tests boundary values for the description field in SignUpForm (1-30 characters). """

        first_name_max_plus = SignUpForm(data={
            'first_name': 'x' * 31,
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        first_name_max = SignUpForm(data={
            'first_name': 'x' * 30,
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        first_name_max_minus = SignUpForm(data={
            'first_name': 'x' * 29,
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        first_name_nom = SignUpForm(data={
            'first_name': 'x' * 15,
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        first_name_min_plus = SignUpForm(data={
            'first_name': 'x' * 2,
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        first_name_min = SignUpForm(data={
            'first_name': 'x' * 1,
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        first_name_min_minus = SignUpForm(data={
            'first_name': 'x' * 0,
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        # first_name_max_plus and first_name_min_minus should return false.
        self.assertFalse(first_name_max_plus.is_valid(), 'Acceptable range: 1-30 characters')
        self.assertFalse(first_name_min_minus.is_valid(), 'Acceptable range: 1-30 characters')

        # first_name_max, first_name_max_minus, first_name_nom,
        # first_name_min_plus, and first_name_min should return true.
        self.assertTrue(first_name_max.is_valid(), 'Acceptable range: 1-30 characters')
        self.assertTrue(first_name_max_minus.is_valid(), 'Acceptable range: 1-30 characters')
        self.assertTrue(first_name_nom.is_valid(), 'Acceptable range: 1-30 characters')
        self.assertTrue(first_name_min_plus.is_valid(), 'Acceptable range: 1-30 characters')
        self.assertTrue(first_name_min.is_valid(), 'Acceptable range: 1-30 characters')

    def test_sign_up_categories(self):
        """ Tests boundary values for the categories field in SignUpForm (minimum one Category-object). """

        categories_min_plus = SignUpForm(data={
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        categories_min = SignUpForm(data={
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        categories_min_minus = SignUpForm(data={
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': [],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'country',
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        # categories_min_minus should return false.
        self.assertFalse(categories_min_minus.is_valid(), 'Minimum one Category-object')

        # categories_min, and categories_min_plus should return true.
        self.assertTrue(categories_min_plus.is_valid(), 'Minimum one Category-object')
        self.assertTrue(categories_min.is_valid(), 'Minimum one Category-object')

    def test_sign_up_country(self):
        """ Tests boundary values for the country field in SignUpForm (1-50 characters). """

        country_max_plus = SignUpForm(data={
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'x' * 51,
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        country_max = SignUpForm(data={
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'x' * 50,
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        country_max_minus = SignUpForm(data={
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'x' * 49,
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        country_nom = SignUpForm(data={
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'x' * 25,
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        country_min_plus = SignUpForm(data={
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'x' * 2,
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        country_min = SignUpForm(data={
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'x' * 1,
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        country_min_minus = SignUpForm(data={
            'first_name': 'first_name',
            'last_name': 'last_name',
            'categories': ['1', '2'],
            'company': 'company',
            'email': 'email@test.no',
            'email_confirmation': 'email@test.no',
            'phone_number': '12345678',
            'country': 'x' * 0,
            'state': 'state',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street_address',
            'username': 'username',
            'password1': 'test_password',
            'password2': 'test_password',
        })

        # country_max_plus and country_min_minus should return false.
        self.assertFalse(country_max_plus.is_valid(), 'Acceptable range: 1-50 characters')
        self.assertFalse(country_min_minus.is_valid(), 'Acceptable range: 1-50 characters')

        # country_max, country_max_minus, country_nom, country_min_plus, and country_min should return true.
        self.assertTrue(country_max.is_valid(), 'Acceptable range: 1-50 characters')
        self.assertTrue(country_max_minus.is_valid(), 'Acceptable range: 1-50 characters')
        self.assertTrue(country_nom.is_valid(), 'Acceptable range: 1-50 characters')
        self.assertTrue(country_min_plus.is_valid(), 'Acceptable range: 1-50 characters')
        self.assertTrue(country_min.is_valid(), 'Acceptable range: 1-50 characters')


class TestOutputCoverage(TestCase):

    def setUp(self):
        # Create user without hashed password.
        self.user_customer = User.objects.create(username='customer', is_active=True)
        self.user_customer.set_password('password')
        self.user_customer.save()
        self.customer = Profile(id=1, user=self.user_customer)

        # Create user without hashed password.
        self.user_project_manager_1 = User.objects.create(username='user_project_manager_1', is_active=True)
        self.user_project_manager_1.set_password('password')
        self.user_project_manager_1.save()
        self.project_manager_1 = Profile(id=2, user=self.user_project_manager_1)

        # Create user without hashed password.
        self.user_project_manager_2 = User.objects.create(username='user_project_manager_2', is_active=True)
        self.user_project_manager_2.set_password('password')
        self.user_project_manager_2.save()
        self.project_manager_2 = Profile(id=3, user=self.user_project_manager_2)

        # Create project.
        self.category = ProjectCategory.objects.create(name='name')
        self.project = Project.objects.create(id=1, user_id=self.customer.user_id, title='title',
                                              description='description', category=self.category)
        self.project.save()

        # Create task with two task offers.
        self.task = Task.objects.create(project=self.project, title='title', description='description', budget=100)
        self.task_offer_1 = TaskOffer.objects.create(id=1, task=self.task, title='title', description='description',
                                                     price=100, offerer_id=self.project_manager_1.user_id)
        self.task_offer_2 = TaskOffer.objects.create(id=2, task=self.task, title='title', description='description',
                                                     price=100, offerer_id=self.project_manager_1.user_id)
        self.task_offer_1.save()
        self.task_offer_2.save()

        self.client = Client()
        self.client.login(username='customer', password='password')

        # URL to project_view with self.project.id as argument.
        self.project_url = reverse('project_view', args='1')

    def test_two_accepted_offers(self):
        offer_accepted = 'a'

        # Accepts two offers for the same task.
        request_1 = self.client.post(self.project_url, {'offer_response': '', 'taskofferid': self.task_offer_1.id,
                                                        'status': offer_accepted, 'feedback': 'feedback'})
        request_2 = self.client.post(self.project_url, {'offer_response': '', 'taskofferid': self.task_offer_2.id,
                                                        'status': offer_accepted, 'feedback': 'feedback'})

        # Makes the server crash due to a get receiving two values instead of one.
        # request_3 = self.client.post(self.project_url)

        # Updates self.task_offer with data from the database.
        self.task_offer_1.refresh_from_db()
        self.task_offer_2.refresh_from_db()

        # Checks that the status of both project offers is set to 'a'.
        # This leads to a failure as only one offer can be accepted for a single task.
        self.assertEqual(self.task_offer_1.status, offer_accepted)
        self.assertEqual(self.task_offer_1.status, self.task_offer_2.status)

        # Checks that both requests gives HTTP Redirect.
        self.assertEqual(request_1.status_code, 302)
        self.assertEqual(request_1.status_code, request_2.status_code)

    def test_invalid_offer_status(self):
        invalid_status = 'y'
        offer_status = self.task_offer_1.status

        # Accepts two offers for the same task.
        request_1 = self.client.post(self.project_url, {'offer_response': '', 'taskofferid': self.task_offer_1.id,
                                                        'status': invalid_status, 'feedback': 'feedback'})

        # Updates self.task_offer with data from the database.
        self.task_offer_1.refresh_from_db()

        # Checks that the status of both project offers is set to 'a'.
        # This leads to a failure as only one offer can be accepted for a single task.
        self.assertEqual(self.task_offer_1.status, offer_status)
        self.assertNotEqual(self.task_offer_1.status, invalid_status)

        # Checks that the request gives HTTP Redirect.
        self.assertEqual(request_1.status_code, 302)


class TestSystemTest(TestCase):
    def setUp(self):
        # Create user without hashed password.
        self.user_customer = User.objects.create(username='customer', is_active=True)
        self.user_customer.set_password('password')
        self.user_customer.save()
        self.customer = Profile(id=1, user=self.user_customer)

        # Create user without hashed password.
        self.user_project_manager = User.objects.create(username='project_manager', is_active=True)
        self.user_project_manager.set_password('password')
        self.user_project_manager.save()
        self.project_manager = Profile(id=2, user=self.user_project_manager)

        # Create user without hashed password.
        self.user_task_participant = User.objects.create(username='task_participant', is_active=True)
        self.user_task_participant.set_password('password')
        self.user_task_participant.save()
        self.task_participant = Profile(id=3, user=self.user_task_participant)

        # Create user without hashed password.
        self.user_not_task_participant = User.objects.create(username='not_task_participant', is_active=True)
        self.user_not_task_participant.set_password('password')
        self.user_not_task_participant.save()
        self.not_task_participant = Profile(id=4, user=self.user_not_task_participant)

        # Create project.
        self.category = ProjectCategory.objects.create(name='name')
        self.project = Project.objects.create(id=1, user_id=self.customer.user_id, title='title',
                                              description='description', category=self.category)
        self.project.save()

        # Create task with a task offer.
        self.task = Task.objects.create(project=self.project, title='title', description='description', budget=100)
        self.task_offer = TaskOffer.objects.create(id=1, task=self.task, title='title', description='description',
                                                   price=100, offerer_id=self.project_manager.user_id)
        self.task_offer.save()

        # Create the client and log in as the customer.
        self.client = Client()
        self.client.login(username='customer', password='password')

        # URL to project_view with self.project.id as argument.
        self.project_url = reverse('project_view', args='1')

    def test_filter_and_sort_invalid_input(self):
        # Price-filter and rating-filter is casted to integers, so inputting anything but integers will cause failure.
        # request = self.client.get(self.project_url, {'apply_filter': '', 'price-filter': '100', 'rating-filter': 'a', 'order-by': 'oldest'})

        # Price-filter and rating-filter is casted to integers, so inputting anything but integers will cause failure.
        # request = self.client.get(self.project_url, {'apply_filter': '', 'price-filter': 'a', 'rating-filter': '4', 'order-by': 'oldest'})

        # Rating-filter accepts both small and large numbers, causing bad outcomes as described in the report.
        request_invalid_rating = self.client.get(self.project_url,
                                                 {'apply_filter': '', 'price-filter': '100', 'rating-filter': '-10',
                                                  'order-by': 'oldest'})

        # Price-filter accepts both negative, causing bad outcomes as described in the report.
        request_invalid_price = self.client.get(self.project_url,
                                                {'apply_filter': '', 'price-filter': '-100', 'rating-filter': '5',
                                                 'order-by': 'oldest'})

        # Order-by accepts all input, causing the sort function to chose sort-by newest.
        request_invalid_sorting = self.client.get(self.project_url,
                                                  {'apply_filter': '', 'price-filter': '100', 'rating-filter': '5',
                                                   'order-by': 'invalid'})

        # Checks that the request is successful with status code 200.
        self.assertEqual(request_invalid_rating.status_code, 200,
                         'Get requests in project_view should render project_view.')
        self.assertEqual(request_invalid_price.status_code, 200,
                         'Get requests in project_view should render project_view.')
        self.assertEqual(request_invalid_sorting.status_code, 200,
                         'Get requests in project_view should render project_view.')

    def test_task_review_invalid_input(self):
        task = Task.objects.create(project=self.project, title='title', description='description', budget=100)

        # Rating accepts any integer value, which can lead to very misleading project manager ratings.
        request_invalid_rating = self.client.post(self.project_url,
                                                  {'task_review': '', 'task_id': str(self.task.id),
                                                   'customer_id': str(self.customer.id),
                                                   'project_manager_id': str(self.project_manager.id), 'rating': '10',
                                                   'description': 'description'})

        # Description field exceeds the max limit of 200.
        # The Rating-object is not created but it does not cause failure.
        request_invalid_description = self.client.post(self.project_url,
                                                       {'task_review': '', 'task_id': str(task.id),
                                                        'customer_id': str(self.customer.id),
                                                        'project_manager_id': str(self.project_manager.id),
                                                        'rating': '10',
                                                        'description': 'x' * 201})

        # Trying to create duplicate Rating objects for the same task causes failure.
        # request_duplicate = self.client.post(self.project_url,
        #                                                   {'task_review': '', 'task_id': str(self.task.id),
        #                                                    'customer_id': str(self.customer.id),
        #                                                    'project_manager_id': str(self.project_manager.id),
        #                                                    'rating': '10',        'description': 'description'})

        # Gets the number of ratings for the task.
        rating_count = Rating.objects.filter(task_id=self.task.id).count()

        # Checks that the rating object is created.
        self.assertEqual(rating_count, 1)

        # Checks that the request is successfully redirecting with status code 302.
        self.assertEqual(request_invalid_rating.status_code, 302,
                         'Post requests in project_view should redirect to project_view.')
        self.assertEqual(request_invalid_description.status_code, 302,
                         'Post requests in project_view should redirect to project_view.')